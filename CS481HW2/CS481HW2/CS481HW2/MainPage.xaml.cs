﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481HW2
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        private void Click7(object sender, System.EventArgs e)
        {
            Entry.Text += "7";
        }
        private void Click8(object sender, System.EventArgs e)
        {
            Entry.Text += "8";
        }
        private void Click9(object sender, System.EventArgs e)
        {
            Entry.Text += "9";
        }
        private void Click4(object sender, System.EventArgs e)
        {
            Entry.Text += "4";
        }
        private void Click5(object sender, System.EventArgs e)
        {
            Entry.Text += "5";
        }
        private void Click6(object sender, System.EventArgs e)
        {
            Entry.Text += "6";
        }
        private void Click1(object sender, System.EventArgs e)
        {
            Entry.Text += "1";
        }
        private void Click2(object sender, System.EventArgs e)
        {
            Entry.Text += "2";
        }
        private void Click3(object sender, System.EventArgs e)
        {
            Entry.Text += "3";
        }
        private void ClickDiv(object sender, System.EventArgs e)
        {
            Entry.Text += "/";
        }
        private void ClickX(object sender, System.EventArgs e)
        {
            Entry.Text += "X";
        }
        private void ClickSub(object sender, System.EventArgs e)
        {
            Entry.Text += "-";
        }
        private void ClickAdd(object sender, System.EventArgs e)
        {
            Entry.Text += "+";
        }
        private void ClickC(object sender, System.EventArgs e)
        {
            Entry.Text = "";
        }
        private void ClickEq(object sender, System.EventArgs e)
        {
            Entry.Text += "=";
        }
        private void Click0(object sender, System.EventArgs e)
        {
            Entry.Text += "0";
        }
    }
}
